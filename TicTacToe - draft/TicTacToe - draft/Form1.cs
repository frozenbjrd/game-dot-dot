﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe___draft
{
    public partial class Form1 : Form
    {
        bool turn = true; //true for X turn, O otherwise
        int turn_count = 0, x_wins = 0, o_wins = 0, draw = 0;
        string winner;
        bool there_is_a_winner = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void instructionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Let me google it for you dumbass <3", "LUL");
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }
            }//end try
            catch { }
            there_is_a_winner = false;
            turn_count = 0;
            turn = true;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("By Tuan, learn from Chris.\nSuper Cool!", "About");
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            turn_count++;
            Button b = (Button)sender;
            
            if (turn)
                b.Text = "X";
            else
                b.Text = "O";
            turn = !turn;
            b.Enabled = false;
            check_for_winner();
        }

        private void check_for_winner()
        {
            if (turn_count == 9)
            {
                MessageBox.Show("Draw!", "Agrr!");
                draw++;
            }
            if (A1.Text == A2.Text && A2.Text == A3.Text && !A1.Enabled)
                there_is_a_winner = true;
            else if (B1.Text == B2.Text && B2.Text == B3.Text && !B1.Enabled)
                there_is_a_winner = true;
            else if (C1.Text == C2.Text && C2.Text == C3.Text && !C1.Enabled)
                there_is_a_winner = true;
            else if (C1.Text == A1.Text && A1.Text == B1.Text && !C1.Enabled)
                there_is_a_winner = true;
            else if (C2.Text == A2.Text && A2.Text == B2.Text && !C2.Enabled)
                there_is_a_winner = true;
            else if (C3.Text == A3.Text && A3.Text == B3.Text && !C3.Enabled)
                there_is_a_winner = true;
            else if (C3.Text == B2.Text && B2.Text == A1.Text && !C3.Enabled)
                there_is_a_winner = true;
            else if (C1.Text == B2.Text && B2.Text == A3.Text && !C1.Enabled)
                there_is_a_winner = true;

            if (there_is_a_winner)
            {
                disable_button();
                if (turn)
                {
                    winner = "O";
                    x_wins++;
                }
                else
                {
                    winner = "X";
                    o_wins++;
                }
                MessageBox.Show(winner + " win!", "Finish him!");
            }
        }

        private void recordToolStripMenuItem_MouseHover(object sender, EventArgs e)
        {

        }

        private void button_enter(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.ForeColor = Color.Red;
                if (turn)
                    b.Text = "X";
                else
                    b.Text = "O";
            }
        }

        private void button_leave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.ForeColor = SystemColors.ControlText;
                if (turn)
                    b.Text = "";
                else
                    b.Text = "";
            }
        }

        private void disable_button()
        {
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }//end foreach
            }//end try
            catch { }
        }

        private void blueScreenErrorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Believe in this?\nGet a fucking live stupid", "Ngu này");
        }

        private void recordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("X wins " + x_wins + " times\n" + "O wins " + o_wins + " times\n" + "Draw " + draw + " times\n", "Statistic");

        }
    }
}
